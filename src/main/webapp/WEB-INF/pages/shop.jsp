﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shop</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->


    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/plugins/plugins.css">

    <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->


    <!-- Main Style CSS (Please use minify version for better website load performance) -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!--<link rel="stylesheet" href="assets/css/style.min.css">-->

</head>

<body>

<!-- Start Header导航栏 菜单                                                              -->
<!-- 导航栏 菜单  -->
<header class="header-area haeder-default  black-logo-version ptb_lg--40 ptb_md--40 ptb_sm--30">
    <div class="header-wrapper container">
        <div class="row align-items-center">
            <!-- Start Header Left -->
            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-6">
                <div class="logo">
                    <!-- LOGO跳转到首页-->
                    <a href="index.html">
                        <img src="assets/images/logo/logo-black.png" alt="Draven logo">
                    </a>
                </div>
            </div >
            <!-- Start Header Center -->
            <div class="col-xl-9 d-none d-xl-block">
                <nav class="mainmenu__nav">
                    <!-- Start Mainmanu Nav -->
                    <ul class="primary-menu megamenu-wrapper">
                        <!--跳转到首页-->
                        <li class="lavel-1"><a href="index.html">首页</a>

                        </li>
                        <!--跳转到关于-->
                        <li><a href="about.html">关于</a></li>
                        <!--跳转到商店-->
                        <li class="lavel-1"><a href="shop.html">商店</a>
                            <ul class="dropdown__menu">

                            </ul>
                        </li>
                        <!--跳转到功能-->
                        <li class="lavel-1"><a href="#">功能</a>
                            <ul class="dropdown__menu">

                                <li class="lavel-1"><a href="#">Pages</a>
                                    <ul class="dropdown__menu">
                                        <!--跳转到购物车-->
                                        <li><a href="cart.html"><span>购物车</span></a></li>
                                        <!--跳转到结账-->
                                        <li><a href="checkout.html"><span>结账</span></a></li>
                                        <!--跳转到收藏-->
                                        <li><a href="wishlist.html"><span>收藏</span></a></li>
                                        <!--跳转到订单-->
                                        <li><a href="order.html"><span>订单</span></a></li>
                                        <!--跳转到个人信息-->
                                        <li><a href="my-account.html"><span>个人信息</span></a></li>

                                        <!--跳转到登录注册-->
                                        <li><a href="login-register.html"><span>登录注册</span></a></li>
                                    </ul>
                                </li>
                                <!--跳转到联系我们-->
                                <li><a href="contact.html">联系我们</a></li>

                            </ul>
                            <!-- End Mainmanu Nav -->
                        </li>
                    </ul>
                </nav>
            </div>
            <!--购物车-->
            <!-- Start Header -->
            <!-- Start Search Flyover -->
            <div class="col-xl-1 col-lg-6 col-md-6 col-sm-6 col-6">
                <div class="header-icon d-flex justify-content-end cart text-right">
                    <a class="cart-trigger" href="#">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="cart-count">03</span>
                    </a>
                    <a class="hamburger-trigger d-block d-xl-none pl--15" href="#">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Start Header -->
<!-- Start Search Flyover -->
<div class="search-flyoveray"></div>
<div class="cart-flyout">
    <div class="cart-flyout-inner">
        <a class="btn-close-cart" href="#">
            <i class="fa fa-times"></i>
        </a>
        <div class="cart-flyout__content">
            <div class="cart-flyout__heading">Shopping Cart</div>
            <div class="widget_shopping_cart_content">
                <ul class="product_list_widget">
                    <li>
                        <div class="thumb">
                            <img src="assets/images/product/sm-product-01.jpg" alt="product">
                        </div>
                        <div class="content">
                            <h6><a href="single-product.html">Boys light blue jacket</a></h6>
                            <div class="quntity">1 × $35.99</div>
                            <button class="remove-btn">×</button>
                        </div>
                    </li>

                    <li>
                        <div class="thumb">
                            <img src="assets/images/product/sm-product-02.jpg" alt="product">
                        </div>
                        <div class="content">
                            <h6><a href="single-product.html">Boys light blue jacket</a></h6>
                            <div class="quntity">1 × $35.99</div>
                            <button class="remove-btn">×</button>
                        </div>
                    </li>

                    <li>
                        <div class="thumb">
                            <img src="assets/images/product/sm-product-03.jpg" alt="product">
                        </div>
                        <div class="content">
                            <h6><a href="single-product.html">Boys light blue jacket</a></h6>
                            <div class="quntity">1 × $35.99</div>
                            <button class="remove-btn">×</button>
                        </div>
                    </li>

                </ul>
            </div>
            <p class="minicart__total">Subtotal: <span class="price">164.97</span></p>
            <div class="cart__btn">
                <a href="cart.html">View cart</a>
                <a href="checkout.html">Checkout</a>
            </div>
        </div>
    </div>
</div>
<!-- ？？？？？？？？ -->
<!-- End Search Flyover -->
<!-- Start Hamburger-->
<div class="hamburger-area">
    <div class="btn-close-search">
        <button>
            <i class="fa fa-times" aria-hidden="true"></i>
        </button>
    </div>
    <!-- Start Main Menu -->
    <ul class="menu-primary-menu-1 responsive-manu d-block d-xl-none" id="responsive-manu">
        <li class="has-dropdown"><a href="index.html">Home</a>
            <ul class="sub-menu">
                <li><a href="index.html"><span>Home 01</span></a></li>
                <li><a href="index-2.html"><span>Home 02</span></a></li>
            </ul>
        </li>
        <li><a href="about.html">About</a></li>
        <li class="has-dropdown"><a href="shop.html">Shop</a>
            <ul class="sub-menu">
                <li><a href="shop.html"><span>Shop</span></a></li>
                <li><a href="shop-left-sidebar.html"><span>Shop Left Sidebar</span></a></li>
                <li><a href="shop-no-sidebar.html"><span>Shop No Sidebar</span></a></li>
                <li><a href="product-details.html"><span>Product Details</span></a></li>
            </ul>
        </li>
        <li class="has-dropdown"><a href="#">Pages</a>
            <ul class="sub-menu">
                <li><a href="cart.html"><span>Cart</span></a></li>
                <li><a href="checkout.html"><span>Checkout</span></a></li>
                <li><a href="wishlist.html"><span>Wishlist</span></a></li>
                <li><a href="compare.html"><span>Compare</span></a></li>
                <li><a href="my-account.html"><span>My Account</span></a></li>
                <li><a href="login-register.html"><span>Login/Register</span></a></li>
            </ul>
        </li>
        <li class="has-dropdown"><a href="blog.html">Blog</a>
            <ul class="sub-menu">
                <li><a href="blog.html"><span>Blog</span></a></li>
                <li><a href="blog-left-sidebar.html"><span>Blog Left Sidebar</span></a></li>
                <li><a href="blog-details.html"><span>Blog Details</span></a></li>
            </ul>
        </li>
        <li><a href="contact.html">Contact</a></li>
    </ul>
    <!-- End Main Menu -->
</div>
<!-- End Hamburger -->

<!--上面的大字：商店 -->
<!-- Start Breadumb Area -->
<div class="breadcumb-area ptb--100 ptb_md--100 ptb_sm--100  bg_image bg_image--3">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner text-center">
                    <h2 class="font--40 mb--0">手办商城</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadumb Area -->
<div class="main-wrapper">
    <!--显示条数 -->
    <!-- Start Shop Area-->
    <div class="shop-area bg-color ptb--120 ptb_md--80 ptb_sm--80" data-bg-color="#ffffff">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="archive-shop-actions mb--30">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <c:if test="${not empty showGKList}">
                                    <div class="archive-shop-inner text-center text-sm-left">
                                        <p class="bk_pra">已显示${showGKList.size()}条数据  </p>
                                    </div>
                                </c:if>
                            </div>
                            <!-- 排序-->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="archive-shop-inner text-center text-sm-right mt_mobile--20">
                                    <select>
                                        <option>Sort by popularity</option>
                                        <option>Sort by average rating</option>
                                        <option>Sort by price: low to high</option>
                                        <option>Sort by price: high to low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Start Shop Minimal Product -->
                    <div class="shop-minimal-product">
                        <div class="row row--25">
                            <!-- 1一个单独的商品-->
                            <!-- Start Single Product -->
                            <c:if test="${empty showGKList}">
                                <h1>没货了，下次请提前一点哦</h1>
                            </c:if>
                            <c:if test="${!empty showGKList}">
                                <c:forEach items="showGKList" var="Gk">
                                </c:forEach>

                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[0].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[0].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[0].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- 2一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[1].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[1].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[1].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- End Single Product -->
                                <!-- 3一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[2].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[2].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[2].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- 4一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[3].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[3].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[3].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- 5一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[4].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[4].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[4].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- 6一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[5].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[5].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[5].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- 7一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[6].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[6].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[6].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[7].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[7].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[7].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- End Single Product -->
                                <!-- 9一个单独的商品-->                            <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                <div class="product">
                                    <div class="inner">
                                        <div class="thumbnail">
                                            <a href="product-details.html">
                                                <img src="${showGKList[8].getGkMjImgId()}
												" alt="Product Images">
                                            </a>
                                        </div>
                                        <div class="product-hover-action">
                                            <div class="hover-inner">
                                                <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h2><a href="product-details.html">${showGKList[8].getGkIp()}</a></h2>
                                        <span class="prize">$${showGKList[8].getGkPrice()}</span>
                                    </div>
                                </div>
                            </div>
                                <!-- End Single Product -->
                                <!--10一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[9].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[9].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[9].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!--11一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[10].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[10].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[10].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- 12一个单独的商品-->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt--60">
                                    <div class="product">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="product-details.html">
                                                    <img src="${showGKList[11].getGkMjImgId()}
												" alt="Product Images">
                                                </a>
                                            </div>
                                            <div class="product-hover-action">
                                                <div class="hover-inner">
                                                    <a title="Quick View" class="quickview" href="#"><i class="fa fa-search"></i></a>
                                                    <a href="cart.html"><i class="fa fa-cart-plus"></i></a>
                                                    <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                    <a href="compare.html"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h2><a href="product-details.html">${showGKList[11].getGkIp()}</a></h2>
                                            <span class="prize">$${showGKList[11].getGkPrice()}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                            </c:if>
                            <!-- End Single Product -->
                            <!-- 页码 -->
                            <!-- Start Pagenation Area -->
                            <div class="col-lg-12">
                                <ul class="pagination_style">
                                    <c:forEach begin="${1}" end="${showGKList.size()}" var="a">
                                        <li > <a href="toShopPageOne?currentPage=${a}">a</a></li>
                                    </c:forEach>

                                </ul>
                            </div>
                            <!-- End Pagenation Area -->

                        </div>
                    </div>
                    <!-- End Shop Minimal Product -->
                </div>
                <div class="col-lg-4 mt_md--60 mt_sm--60">
                    <!-- Start Sidebar Area -->
                    <div class="shop-sidebar-container">
                        <div class="shop-sidebar-wrapper">
                            <!-- Start Single Widget -->
                            <!-- 搜索 -->
                            <div class="shop-sidebar search">
                                <h5 class="widget-title">搜索</h5>
                                <form class="inner" action="searchByAdd">
                                    <div class="search-box">
                                        <input type="text" placeholder="请输入手办ip或者角色名" name="add">
                                        <button><span class="fa fa-search"></span></button>
                                    </div>
                                </form>
                            </div>
                            <!-- End Single Widget -->

                            <!-- 价格过滤 -->
                            <!-- Start Single Widget -->
                            <div class="shop-sidebar related-product-inner mt--50">
                                <h5 class="widget-title">价格过滤</h5>

                                <div class="content-shopby">
                                    <div class="price_filter s-filter clear">

                                        <div id="slider-range"></div>
                                        <div class="slider__range--output">
                                            <div class="price__output--wrap">
                                                <div class="price--output">
                                                    <span>从</span><input type="text" name="minPrice">到<input type="text" name="maxPrice">
                                                </div>
                                                <div class="price--filter">
                                                    <a href="searchByTag?minPrice=${minPrice}&&maxPrice=${maxPrice}">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Widget -->

                            <!-- 分类 -->
                            <!-- Start Single Widget -->
                            <div class="shop-sidebar tag mt--50">
                                <h5 class="widget-title">Tags</h5>
                                <div class="inner">
                                    <ul class="tagcloud">
                                        <c:forEach items="${IpList}" var="ip" >
                                            <li><a href="searchByTag?ip=${ip}">${ip}</a> </li>
                                        </c:forEach>
                                    </ul>
                                </div>

                            </div>



                            <!-- Quick View Modal -->
                            <div class="quick-view-modal">
                                <div class="quick-view-modal-inner">
                                    <div class="container">
                                        <div class="product-details">
                                            <!-- Product Details Left -->
                                            <div class="product-details-left">
                                                <div class="product-details-images slider-navigation-2">
                                                    <a href="#">
                                                        <img class="w-100" src="assets/images/product/product-01.jpg" alt="product image">
                                                    </a>
                                                    <a href="#">
                                                        <img class="w-100" src="assets/images/product/product-02.jpg" alt="product image">
                                                    </a>
                                                    <a href="#">
                                                        <img class="w-100" src="assets/images/product/product-03.jpg" alt="product image">
                                                    </a>
                                                    <a href="#">
                                                        <img class="w-100" src="assets/images/product/product-04.jpg" alt="product image">
                                                    </a>
                                                    <a href="#">
                                                        <img class="w-100" src="assets/images/product/product-05.jpg" alt="product image">
                                                    </a>
                                                </div>
                                                <div class="product-details-thumbs slider-navigation-2">
                                                    <img src="assets/images/product/product-01.jpg" alt="product image thumb">
                                                    <img src="assets/images/product/product-02.jpg" alt="product image thumb">
                                                    <img src="assets/images/product/product-03.jpg" alt="product image thumb">
                                                    <img src="assets/images/product/product-04.jpg" alt="product image thumb">
                                                    <img src="assets/images/product/product-05.jpg" alt="product image thumb">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>













                <!-- JS
                ============================================ -->

                <!-- Modernizer JS -->
                <script src="assets/js/vendor/modernizr.min.js"></script>
                <!-- jQuery JS -->
                <script src="assets/js/vendor/jquery.js"></script>
                <!-- Bootstrap JS -->
                <script src="assets/js/vendor/bootstrap.min.js"></script>
                <script src="assets/js/plugins/plugins.js"></script>

                <script src="assets/js/main.js"></script>

            </div>
        </div></div></div></body>


</html>

