package com.project.ssm.service;

import com.project.ssm.model.User;

public interface UserService {
    public User login(String userName);

    public void register(User user);
}
