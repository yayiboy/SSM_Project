package com.project.ssm.service;

import com.project.ssm.model.CarObject;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public interface CarService {
    @RequestMapping("toCar")
    public List<CarObject> toCar(String uid) throws Exception;
}
