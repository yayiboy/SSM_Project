package com.project.ssm.service;

import com.project.ssm.model.Gk;
import org.springframework.stereotype.Service;

import java.util.List;


public interface GKService {
     public List<Gk> findAllGK();

     public List<Gk> findAllByAdd(String add);

     public List<Gk> findAllByTag(String gkId);

     public List<Gk> findAllByPrice(double max,double min);
}
