package com.project.ssm.service.Impl;

import com.project.ssm.dao.GKMapper;
import com.project.ssm.model.Gk;
import com.project.ssm.service.GKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class GKServiceImpl implements GKService {
    @Autowired
    private GKMapper gkMapper;

    @Override
    public List<Gk> findAllGK() {
        return gkMapper.findAllGK();
    }

    //    @Override
//    public Page<User> findByPage(int currentPage) throws Exception {
//        HashMap<String,Object> map = new HashMap<String,Object>();
//        Page<User> pageBean = new Page<User>();
//
//        //封装当前页数
//        pageBean.setCurrPage(currentPage);
//
//        //每页显示的数据
//        int pageSize=5;
//        pageBean.setPageSize(pageSize);
//
//        //封装总记录数
//        int totalCount = gkMapper.findTotal();
//        pageBean.setTotalCount(totalCount);
//
//        //封装总页数
//        double tc = totalCount;
//        Double num =Math.ceil(tc/pageSize);//向上取整
//        pageBean.setTotalPage(num.intValue());
//        map.put("start",(currentPage-1)*pageSize);
//        map.put("size", pageBean.getPageSize());
//        //封装每页显示的数据
//        List<com.sample.Gk> lists = gkMapper.findByPage(map);
//        pageBean.setLists(lists);
//
//        return pageBean;
//    }


    @Override
    public List<Gk> findAllByAdd(String add) {
        add = "%"+add+"%";
        return gkMapper.findAllByAdd(add);
    }

    @Override
    public List<Gk> findAllByTag(String gkId) {
        return gkMapper.findAllByTag(gkId);
    }

    @Override
    public List<Gk> findAllByPrice(double max, double min) {
        return gkMapper.findAllByPrice(max,min);
    }
}
