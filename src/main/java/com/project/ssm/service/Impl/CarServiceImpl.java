package com.project.ssm.service.Impl;

import com.project.ssm.dao.CarMapper;
import com.project.ssm.dao.GKMapper;
import com.project.ssm.model.Car;
import com.project.ssm.model.CarObject;
import com.project.ssm.model.Gk;
import com.project.ssm.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
@Service
public class CarServiceImpl implements CarService {
    @Autowired
    CarMapper carMapper;
    @Autowired
    GKMapper gkMapper;
    @Override
    public List<CarObject> toCar(String uid) throws Exception {
        List<Car> carlist=carMapper.selectCarByUserId(uid);
        if(!carlist.isEmpty()){
        List<CarObject> objectlist=new LinkedList<CarObject>();
      for(int a=0;a<carlist.size();a++) {
          Car car = carlist.get(a);
          Gk gk = (Gk) gkMapper.findAllById(car.getWareId());
          //封装一个购物车显示单元的POJO类
          CarObject carObject = new CarObject(gk.getGkMjImgId(), gk.getGkRole(), gk.getGkPrice(), car.getWareNum(), car.getWareNum() * gk.getGkPrice());
          objectlist.add(carObject);
      }

      }
        return null;
    }
}
