package com.project.ssm.service.Impl;

import com.project.ssm.dao.UserMapper;
import com.project.ssm.model.User;
import com.project.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(String userName) {
        return userMapper.findUserByName(userName);
    }

    @Override
    public void register(User user) {
        userMapper.register(user);
        return ;
    }
}
