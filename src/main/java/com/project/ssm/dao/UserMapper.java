package com.project.ssm.dao;

import com.project.ssm.model.User;
import org.springframework.stereotype.Repository;

@Repository("userMapper")
public interface UserMapper {
    User findUserByName(String userName);

    void register(User user);
}
