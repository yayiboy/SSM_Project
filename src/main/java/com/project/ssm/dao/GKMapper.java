package com.project.ssm.dao;


import com.project.ssm.model.Gk;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.StreamHandler;

@Repository("gkMapper")
public interface GKMapper {
    public List<Gk> findAllGK();

    public List<Gk> findAllByAdd(String add);

    public List<Gk> findAllByTag(String gkId);

    public List<Gk> findAllByPrice(double max, double min);
    public List<Gk> findAllById(String gk_id);
}
