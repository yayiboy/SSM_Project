package com.project.ssm.dao;

import com.project.ssm.model.Car;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository("carMapper")
public interface CarMapper {
    public List<Car> selectCarByUserId(String uid) throws  Exception;
}
