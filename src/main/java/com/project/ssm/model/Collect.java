package com.sample;


public class Collect {

  private String collId;
  private String userId;
  private String wareId;
  private String tt;


  public String getCollId() {
    return collId;
  }

  public void setCollId(String collId) {
    this.collId = collId;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getWareId() {
    return wareId;
  }

  public void setWareId(String wareId) {
    this.wareId = wareId;
  }


  public String getTt() {
    return tt;
  }

  public void setTt(String tt) {
    this.tt = tt;
  }

}
