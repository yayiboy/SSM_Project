package com.sample;


public class Comment {

  private String comId;
  private String userId;
  private String managerId;
  private String comContent;
  private java.sql.Timestamp comTime;
  private String tt;
  private String dd;


  public String getComId() {
    return comId;
  }

  public void setComId(String comId) {
    this.comId = comId;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getManagerId() {
    return managerId;
  }

  public void setManagerId(String managerId) {
    this.managerId = managerId;
  }


  public String getComContent() {
    return comContent;
  }

  public void setComContent(String comContent) {
    this.comContent = comContent;
  }


  public java.sql.Timestamp getComTime() {
    return comTime;
  }

  public void setComTime(java.sql.Timestamp comTime) {
    this.comTime = comTime;
  }


  public String getTt() {
    return tt;
  }

  public void setTt(String tt) {
    this.tt = tt;
  }


  public String getDd() {
    return dd;
  }

  public void setDd(String dd) {
    this.dd = dd;
  }

}
