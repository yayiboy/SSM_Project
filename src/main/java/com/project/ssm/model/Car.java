package com.project.ssm.model;


public class Car {

  private String carId;
  private String userId;
  private String wareId;
  private long wareNum;
  private String imId;
  private String tt;


  public String getCarId() {
    return carId;
  }

  public void setCarId(String carId) {
    this.carId = carId;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getWareId() {
    return wareId;
  }

  public void setWareId(String wareId) {
    this.wareId = wareId;
  }


  public long getWareNum() {
    return wareNum;
  }

  public void setWareNum(long wareNum) {
    this.wareNum = wareNum;
  }


  public String getImId() {
    return imId;
  }

  public void setImId(String imId) {
    this.imId = imId;
  }


  public String getTt() {
    return tt;
  }

  public void setTt(String tt) {
    this.tt = tt;
  }

  public Car(String carId, String userId, String wareId, long wareNum, String imId, String tt) {
    this.carId = carId;
    this.userId = userId;
    this.wareId = wareId;
    this.wareNum = wareNum;
    this.imId = imId;
    this.tt = tt;
  }
}
