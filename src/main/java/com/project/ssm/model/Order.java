package com.sample;


public class Order {

  private String orderId;
  private String userId;
  private String orderNum;
  private String orderCreTime;
  private long wareTtNum;
  private double wareTtPrice;
  private String orderState;
  private String userAddr;


  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getOrderNum() {
    return orderNum;
  }

  public void setOrderNum(String orderNum) {
    this.orderNum = orderNum;
  }


  public String getOrderCreTime() {
    return orderCreTime;
  }

  public void setOrderCreTime(String orderCreTime) {
    this.orderCreTime = orderCreTime;
  }


  public long getWareTtNum() {
    return wareTtNum;
  }

  public void setWareTtNum(long wareTtNum) {
    this.wareTtNum = wareTtNum;
  }


  public double getWareTtPrice() {
    return wareTtPrice;
  }

  public void setWareTtPrice(double wareTtPrice) {
    this.wareTtPrice = wareTtPrice;
  }


  public String getOrderState() {
    return orderState;
  }

  public void setOrderState(String orderState) {
    this.orderState = orderState;
  }


  public String getUserAddr() {
    return userAddr;
  }

  public void setUserAddr(String userAddr) {
    this.userAddr = userAddr;
  }

}
