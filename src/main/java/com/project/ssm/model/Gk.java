package com.project.ssm.model;


import java.sql.Timestamp;
import java.util.Date;

public class Gk {

  private String gkId;
  private String gkIp;
  private String gkBrand;
  private Timestamp gkOnSellTime;
  private double gkPrice;
  private long gkStock;
  private String gkRole;
  private String gkStuff;
  private String gkStandard;
  private String gkMjImgId;
  private String gkDataImgId;
  private String gkContain;
  private String tt;


  public String getGkId() {
    return gkId;
  }

  public void setGkId(String gkId) {
    this.gkId = gkId;
  }


  public String getGkIp() {
    return gkIp;
  }

  public void setGkIp(String gkIp) {
    this.gkIp = gkIp;
  }


  public String getGkBrand() {
    return gkBrand;
  }

  public void setGkBrand(String gkBrand) {
    this.gkBrand = gkBrand;
  }


  public java.sql.Timestamp getGkOnSellTime() {
    return gkOnSellTime;
  }

  public void setGkOnSellTime(java.sql.Timestamp gkOnSellTime) {
    this.gkOnSellTime = gkOnSellTime;
  }


  public double getGkPrice() {
    return gkPrice;
  }

  public void setGkPrice(double gkPrice) {
    this.gkPrice = gkPrice;
  }


  public long getGkStock() {
    return gkStock;
  }

  public void setGkStock(long gkStock) {
    this.gkStock = gkStock;
  }


  public String getGkRole() {
    return gkRole;
  }

  public void setGkRole(String gkRole) {
    this.gkRole = gkRole;
  }


  public String getGkStuff() {
    return gkStuff;
  }

  public void setGkStuff(String gkStuff) {
    this.gkStuff = gkStuff;
  }


  public String getGkStandard() {
    return gkStandard;
  }

  public void setGkStandard(String gkStandard) {
    this.gkStandard = gkStandard;
  }


  public String getGkMjImgId() {
    return gkMjImgId;
  }

  public void setGkMjImgId(String gkMjImgId) {
    this.gkMjImgId = gkMjImgId;
  }


  public String getGkDataImgId() {
    return gkDataImgId;
  }

  public void setGkDataImgId(String gkDataImgId) {
    this.gkDataImgId = gkDataImgId;
  }


  public String getGkContain() {
    return gkContain;
  }

  public void setGkContain(String gkContain) {
    this.gkContain = gkContain;
  }


  public String getTt() {
    return tt;
  }

  public void setTt(String tt) {
    this.tt = tt;
  }

  @Override
  public String toString() {
    return this.getGkId()+":"+this.getGkRole();
  }
}
