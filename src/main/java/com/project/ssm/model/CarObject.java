package com.project.ssm.model;

public class CarObject {
    private String img;
    private String role;
    private Double price;
    private long quuantity;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public long getQuuantity() {
        return quuantity;
    }

    public void setQuuantity(long quuantity) {
        this.quuantity = quuantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public CarObject(String img, String role, Double price, long quuantity, Double total) {
        this.img = img;
        this.role = role;
        this.price = price;
        this.quuantity = quuantity;
        this.total = total;
    }

    private Double total;


}
