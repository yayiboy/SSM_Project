package com.sample;


public class Image {

  private String imId;
  private String imAddr;
  private String gkId;
  private long majorIm;


  public String getImId() {
    return imId;
  }

  public void setImId(String imId) {
    this.imId = imId;
  }


  public String getImAddr() {
    return imAddr;
  }

  public void setImAddr(String imAddr) {
    this.imAddr = imAddr;
  }


  public String getGkId() {
    return gkId;
  }

  public void setGkId(String gkId) {
    this.gkId = gkId;
  }


  public long getMajorIm() {
    return majorIm;
  }

  public void setMajorIm(long majorIm) {
    this.majorIm = majorIm;
  }

}
