package com.sample;


public class Manager {

  private String managerId;
  private String managerName;
  private String managerPsd;
  private String managerPhone;
  private String managerSex;
  private String tt;


  public String getManagerId() {
    return managerId;
  }

  public void setManagerId(String managerId) {
    this.managerId = managerId;
  }


  public String getManagerName() {
    return managerName;
  }

  public void setManagerName(String managerName) {
    this.managerName = managerName;
  }


  public String getManagerPsd() {
    return managerPsd;
  }

  public void setManagerPsd(String managerPsd) {
    this.managerPsd = managerPsd;
  }


  public String getManagerPhone() {
    return managerPhone;
  }

  public void setManagerPhone(String managerPhone) {
    this.managerPhone = managerPhone;
  }


  public String getManagerSex() {
    return managerSex;
  }

  public void setManagerSex(String managerSex) {
    this.managerSex = managerSex;
  }


  public String getTt() {
    return tt;
  }

  public void setTt(String tt) {
    this.tt = tt;
  }

}
