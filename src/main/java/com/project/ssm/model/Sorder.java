package com.sample;


public class Sorder {

  private String orderId;
  private String orderName;
  private String wareId;
  private String wareNum;
  private String imId;
  private double onePrice;
  private double ttPrice;
  private String tt;


  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }


  public String getOrderName() {
    return orderName;
  }

  public void setOrderName(String orderName) {
    this.orderName = orderName;
  }


  public String getWareId() {
    return wareId;
  }

  public void setWareId(String wareId) {
    this.wareId = wareId;
  }


  public String getWareNum() {
    return wareNum;
  }

  public void setWareNum(String wareNum) {
    this.wareNum = wareNum;
  }


  public String getImId() {
    return imId;
  }

  public void setImId(String imId) {
    this.imId = imId;
  }


  public double getOnePrice() {
    return onePrice;
  }

  public void setOnePrice(double onePrice) {
    this.onePrice = onePrice;
  }


  public double getTtPrice() {
    return ttPrice;
  }

  public void setTtPrice(double ttPrice) {
    this.ttPrice = ttPrice;
  }


  public String getTt() {
    return tt;
  }

  public void setTt(String tt) {
    this.tt = tt;
  }

}
