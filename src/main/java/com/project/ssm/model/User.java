package com.project.ssm.model;


public class User {

  private String userId;
  private String userName;
  private String userPsd;
  private String userSex;
  private String userBir;
  private String userPhone;
  private String userAddr;
  private String userEmail;
  private String userHeadId;
  private String temp1;
  private String temp2;


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }


  public String getUserPsd() {
    return userPsd;
  }

  public void setUserPsd(String userPsd) {
    this.userPsd = userPsd;
  }


  public String getUserSex() {
    return userSex;
  }

  public void setUserSex(String userSex) {
    this.userSex = userSex;
  }


  public String getUserBir() {
    return userBir;
  }

  public void setUserBir(String userBir) {
    this.userBir = userBir;
  }


  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }


  public String getUserAddr() {
    return userAddr;
  }

  public void setUserAddr(String userAddr) {
    this.userAddr = userAddr;
  }


  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }


  public String getUserHeadId() {
    return userHeadId;
  }

  public void setUserHeadId(String userHeadId) {
    this.userHeadId = userHeadId;
  }


  public String getTemp1() {
    return temp1;
  }

  public void setTemp1(String temp1) {
    this.temp1 = temp1;
  }


  public String getTemp2() {
    return temp2;
  }

  public void setTemp2(String temp2) {
    this.temp2 = temp2;
  }

  @Override
  public String toString() {
    return userId+":"+userName+"login success!!!";
  }
}
