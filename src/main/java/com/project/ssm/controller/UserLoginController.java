package com.project.ssm.controller;

import com.project.ssm.model.User;
import com.project.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.UUID;

@Controller
//这里用了@SessionAttributes，可以直接把model中的user(也就key)放入其中
//这样保证了session中存在user这个对象
@SessionAttributes("user")
public class UserLoginController {

    @Autowired
    private UserService userService;

    //用户登录界面
    @RequestMapping("userLogin")
    public String userLogin(String userName, String userPsd, ModelMap modelMap, HttpServletRequest request){
        HttpSession session = request.getSession();
        System.out.print("[service传来消息]:"+userName+"密码为："+userPsd+"的情况为：");
        if(!userName.equals("") && !userPsd.equals("")){    //判断用户名和密码是否为空
            if(userService.login(userName) != null){        //判断是否存在此用户名
                System.out.print("存在！！");
                if(userService.login(userName).getUserPsd().equals(userPsd)){//判断此密码是否正确匹配
                    System.out.println("并且成功登录！！！");
                    //将登录的用户设置为user
                    modelMap.addAttribute("user",userService.login(userName));
                    session.setAttribute("user",userService.login(userName));
                    return "forward:index.jsp";
                }else {
                    modelMap.put("msg","密码错误！！");
                    System.out.println("但是密码错了");
                    return "login-register";
                }

            }else{
                modelMap.put("msg","用户名不存在！！");
                System.out.println("不存在");
                return "login-register";
            }
        }else{
            modelMap.put("msg","请填写完整用户名和密码！！");
            System.out.println("瞎搞");
            return "login-register";
        }
    }

    //跳转到注册界面===========
    @RequestMapping("toRegister")
    public String toRegister(){

        System.out.println("跳，都给我条！！！");
        return "login-register";
    }

    //检查注册信息是否填写完整以及密码确定是否正确
    @RequestMapping("checkRegister")
    public String userRegister(String userPsdSure,User user, ModelMap modelMap){
        //判断是否填写完整
        if(
                user.getUserName().equals("")
                || user.getUserPsd().equals("")
                || user.getUserAddr().equals("")
                || user.getUserBir().equals("")
                || user.getUserPhone().equals("")
                || user.getUserSex().equals("")
                || user.getUserEmail().equals("")
                || userPsdSure.equals("")
        ){
            modelMap.put("registerMsg","请填写完整！！！");
            return "login-register";
        }

        //判断密码是否确定
        if(!userPsdSure.equals(user.getUserPsd())){
            modelMap.put("registerMsg","两次密码不一致！！！");
            return "login-register";
        }

        //判断用户名是否存在
        if(userService.login(user.getUserName())!=null){
            modelMap.put("registerMsg","用户名已存在！！！");
            return "login-register";
        }

        //其他判断

        //符合条件，开始注册
        return "forward:userRegister";
    }

    //开始真正注册
    @RequestMapping("userRegister")
    public String toRegister(User user,ModelMap modelMap){
        String userId = UUID.randomUUID().toString().replaceAll("-","");
        user.setUserId(userId);
        userService.register(user);
        modelMap.put("registerMsg","注册成功！！请登录");
        return "login-register";
    }
}
