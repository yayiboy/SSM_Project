package com.project.ssm.controller;

import com.project.ssm.service.GKService;
import com.project.ssm.service.UserService;
import org.apache.ibatis.annotations.ResultMap;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.project.ssm.model.*;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
public class GKController {
    @Autowired
    private GKService gkService;

    //接受跳转到商品展示页面的响应

    /**
     * 这里将所有的商品查询出来，并且放到GKList
     * 同时可以响应分页，将分页之后当前页面的商品放到showGKList
     * 返回是到本页面
     * @param
     * @param model
     * @return
     */
    @RequestMapping("toShopPageOne")
    public String toShop( Model model,HttpServletRequest request){
        System.out.println("zhangnoa-----------------------------------");
        HttpSession session = request.getSession(true);
//            model.addAttribute("pagemsg", GKService.findByPage(1));
            //获得所有的gk对象
            List<Gk> GKList = gkService.findAllGK();
            //设置GKList
            session.setAttribute("GKList",GKList);
            //设置展示页面总数
            session.setAttribute("GKColumnCount",Math.ceil(GKList.size()/12.0));
            //根据当前页面数来展示内容
        System.out.println("22222222222222222222222222" );
        System.out.println("gl"+GKList.isEmpty());
        //设置当前页面
        session.setAttribute("nowPage",1);
//            int nowPage = currentPage;//获取当前页面是第几个
//            List<Gk> showGKList = new LinkedList<>();
//            //添加确定的Gk到showGKList里面
//            if(GKList.size()>0 && GKList != null){
//                for(int i = (nowPage-1)*12; i < nowPage*12; i++){
//                    showGKList.add(GKList.get(i));
//                }
//            }
//            model.addAttribute("showGKList",showGKList);//设置展示页面的list
//            //看看都有啥
//
//            for(int i = 0; i < showGKList.size(); i++){
//                System.out.println(showGKList.get(i).toString());;
//            }

        return "forward:choosePage";
    }


    //此处处理分页的list，并且传递
    /**
     * 所有查询之后的数据都要传过这里来
     * @return
     */
    @RequestMapping("choosePage")
    public String choosePage(@RequestParam(value="currentPage",defaultValue="1",required=false)int currentPage,HttpServletRequest request){
        HttpSession session = request.getSession();
        List<Gk> GKList = (List<Gk>) session.getAttribute("GKList");
        int nowPage = currentPage;//获取当前页面是第几个
        List<Gk> showGKList = new LinkedList<>();
        //添加确定的Gk到showGKList里面
        if(GKList.size()>0 && GKList != null){
            for(int i = (nowPage-1)*12; i < nowPage*12&& i<GKList.size(); i++){
                showGKList.add(GKList.get(i));
            }
        }
        session.setAttribute("showGKList",showGKList);//设置展示页面的list

        //把tag标签的IpList确定
        List<String> IpList = new ArrayList<>();
        Set<String> IpSet = new HashSet<>();
        for(int i = 0; i < GKList.size(); i++){
            IpSet.add(GKList.get(i).getGkIp());
        }
        IpList.addAll(IpSet);
        session.setAttribute("IpList",IpList);

        //看看showGKList都有啥
        System.out.println("showGKList里面有这些：");
        for(int i = 0; i < showGKList.size(); i++){
            System.out.println(showGKList.get(i).toString());
        }
        //看看IpList里都有啥
        System.out.println("IpList里面有这些：");
        for(int i = 0; i < IpList.size(); i++){
            System.out.println();
        }

        return "shop";
    }


    //下面是响应查询操作

    //根据用户输入查询
    @RequestMapping("searchByAdd")
    public String selectByAddd(String add, HttpServletRequest request){
        HttpSession session = request.getSession();
        if(add == null){
            return "forward:choosePage";
        }
        //获得符合的gk对象,放入
        List<Gk> GKList = gkService.findAllByAdd(add);
        session.setAttribute("GKList",GKList);

        return "forward:choosePage";
    }

    //根据tag标签查询
    @RequestMapping("searchByTag")
    public String selectByTag(String ip,HttpServletRequest request){
        HttpSession session = request.getSession();
        System.out.println(ip);
        //获得符合的GK对象，放入
        List<Gk> GKList = gkService.findAllByTag(ip);
        session.setAttribute("GKList",GKList);

        return "forward:choosePage";
    }

    //根据价格区间查询
    @RequestMapping("searchByPrice")
    public String selectByPrice(String minPrice, String maxPrice, HttpServletRequest request){
        HttpSession session = request.getSession();
        //字符转化，同时定义大小
        double max = Double.parseDouble(maxPrice);
        double min = Double.parseDouble(minPrice);
        if(min > max){
            double a = min;
            min = max;
            max = a;
        }
        System.out.println("max:"+maxPrice+"== min:"+minPrice);
        List<Gk> GKList = gkService.findAllByPrice(max,min);
        session.setAttribute("GkList",GKList);
        return "forward:choosePage";
    }

}

